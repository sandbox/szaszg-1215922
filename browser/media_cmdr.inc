<?php
$plugin = array(
  'weight' => 0,
  'callback' => 'media_cmdr_form',
  'title' => t('MC'),
  'access callback' => 'media_commander_browser_access',
);

/**
 * Access callback to determine whether users should have access to the tab.
 */
function media_commander_browser_access() {
  return user_access('administer media') || user_access('use media commander');
}

/**
 *  Provides a form for adding media items from 3rd party sources.
 */
function media_cmdr_form($form, &$form_state = array(), $types = NULL) {
  $form['media_cmdr'] = array(
        '#title' => t('MC'),
        '#settings' => array(
         'viewMode' => 'thumbnails',
         'getMediaUrl' => url('media/browser/list'),
        ),
#        'form' => array(drupal_get_form('_media_cmdr_form')),
        'form' => _media_cmdr_form(),
      );
  return $form;

  $form['embed_code'] = array(
    '#type' => 'textfield',
    '#title' => 'URL or Embed code',
    '#description' => 'Input a url or embed code from one of the listed providers.',
    '#attributes' => array('class' => array('media-add-from-url')),
    // There is no standard specifying a maximum length for a URL. Internet
    // Explorer supports upto 2083 (http://support.microsoft.com/kb/208427), so
    // we assume publicly available media URLs are within this limit.
    '#maxlength' => 2083,
  );


  // @todo:
  // Add live previews back (currently broken)

  //$form['preview'] = array(
  //  '#type' => 'item',
  //  '#title' => t('Preview'),
  //  '#markup' => '<div id="media-add-from-url-preview"></div>'
  //);

  $form['#validators'] = array();
  if ($types) {
    $form['#validators']['media_file_validate_types'] = array($types);
  }

  $form['providers'] = array();
  $form['providers']['header'] = array('#markup' => '<h2> Supported Providers </h2>');
  foreach (media_internet_get_providers() as $key => $provider) {
    if (empty($provider['hidden']) || $provider['hidden'] != TRUE) {
      if (isset($provider['image'])) {
        $form['providers'][$key] = array('#markup' => theme('image', array('path' => $provider['image'], 'title' => $provider['title'])));
      } else {
        $form['providers'][$key] = array('#markup' => $provider['title']);
      }
      // Wrap the provider in a div so we can make a nice list
      $form['providers'][$key]['#prefix'] = '<div class="media-provider">';
      $form['providers'][$key]['#suffix'] = '</div>';
    }
  }
  if (count($form['providers']) == 1) {
    // Just the header, no actual providers
    unset($form['providers']['header']);
  }
}
