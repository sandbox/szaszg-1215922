<?php

/**
 * @file
 * This file contains the  AJAX Callback function to MC
 */

function _media_cmdr_ajax() {
  $params = drupal_get_query_parameters($_POST);

  switch ($params['op']) {
  case 'list':
    drupal_json_output(_media_cmdr_list_files($params));
    break;
  case 'copy':
    drupal_json_output($params);
    break;
  case 'touch':
    $file = file_uri_to_object($params['uri'], TRUE);

    if (!isset($file->fid)) {
      file_save($file);
      $file = file_uri_to_object($params['uri'], TRUE);
      drupal_set_message(t('%filename has been registered as managed file.', array('%filename' => $file->filename)));
    }

    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'file');
    $query->propertyCondition('uri', $file->uri, '=');
    $query->propertyCondition('status', FILE_STATUS_PERMANENT);
    $result = $query->execute();

    $files = file_load_multiple(array_keys($result['file']));
    foreach ($files as $file) {
      media_browser_build_media_item($file);
    }
    drupal_json_output(array('media' => array_values($files)));
    break;
  default:
    drupal_json_output(array('error' => 'Unknown operation!'));
    break;
  }
  exit();
}

function _media_cmdr_build_list(&$list, &$params) {
  $hfiles = array(); //hidden files
  $hfolders = array(); //hidden folders
  $files = array();
  $folders = array();
  foreach ($list as $k => $file) {
    if (!is_numeric($k)) continue;
    if (!fnmatch($params['fltr'], $file[0])) continue;  //filter by pattern

    if ($file[0] == '..' or $file[0] == '.') continue;
    if ($params['hddn'] == 'false' and $file[0][0] == '.') continue;
    $data = array($file[0]);

    if (mb_strlen($file[0]) > 19)
      $data[] = preg_replace('/^(.{11}).*(.{7})$/', '\1~\2', $file[0]);
    else
      $data[] = $file[0];

    $extension = mb_strpos($file, '.') !== FALSE ?  preg_replace('/^.*\./', '', $file) : '';
    if ($params['sort'] == 'e') $file[0] = $extension . "\001" . $file[0];

    //filesize byte/kilo/mega
    $tmp = preg_replace('/^(.[0-9]*)(\.[0-9])?.*/', '\1\2', $file[1]);
    if (preg_match('/k$/i', $file[1])) $tmp *= 1024;
    if (preg_match('/m$/i', $file[1])) $tmp *= 1024 * 1024;
    if (is_numeric($tmp)) $tmp = round($tmp);
    if ($params['sort'] == 's') $file = sprintf('%08x', $tmp) . $file[0];
    if ($tmp > 1 * 1024 * 1024 * 1024) $tmp = round($tmp / 1024 / 1024) . 'M';
    elseif ($tmp > 8 * 1024 * 1024) $tmp = round($tmp / 1024) . 'K';
    $data[] = $tmp;

    ///modification time
    $tmp = $file[2];
    if ($params['sort'] == 't') $file = sprintf('%08x', $tmp) . $file[0];
    if (abs(time() - $tmp) < 180 * 24 * 60 * 60 ) $tmp = date('m-d H:i', $tmp);
    else $tmp = date('Y-m-d', $tmp);
    $data[] = $tmp;

    $tmp = $file[3] ? 'r' : '-';
    $tmp .= $file[4] ? 'w' : '-';
    $tmp .= $file[5] ? 'm' : '-';
    if (!fnmatch(str_replace('*', '?', $params['rwm']), $tmp)) continue;  //filter by mode
    $data[] = $tmp;

    if ($file[6]) {
      $data[] = 'dir';
      if ($file[0][0] == '.')
        $hfolders[] = $data;
      else
        $folders[] = $data;
    }
    else {
      $data[] = _media_cmdr_class($extension);
      if ($file[0][0] == '.')
        $hfiles[] = $data;
      else
        $files[] = $data;
    }
  }

/*
    if ($params['locale'] !== NULL) {
      $old_locale = setlocale(LC_ALL, 0);
      setlocale(LC_ALL, $params['locale']);
    }
*/

  if ($params['sort'] != 'u') {
    if ($params['sort'] == 's') {
    }
    if ($params['case'] == 'true') {
      ksort($hfolders, SORT_LOCALE_STRING); ksort($hfiles, SORT_LOCALE_STRING);
      ksort($folders, SORT_LOCALE_STRING); ksort($files, SORT_LOCALE_STRING);
    }
    else {
      uksort($hfolders, 'strcasecoll'); uksort($hfiles, 'strcasecoll');
      uksort($folders, 'strcasecoll'); uksort($files, 'strcasecoll');
    }
    if ($params['rvrs'] == 'true') {
      $hfolders = array_reverse($hfolders); $hfiles = array_reverse($hfiles);
      $folders = array_reverse($folders); $files = array_reverse($files);
    }
  }
/*
    if ($params['locale'] !== NULL) setlocale(LC_ALL, $old_locale);
*/
  if (mb_strlen($list['dir']) > 43)
    $dir1 = preg_replace('/^(.{20}).*(.{22})$/', '\1~\2', $list['dir']);
  else
    $dir1 = $list['dir'];
  return array('dir' => array($list['dir'], $dir1), 'pass' => $list['pass'],
    'folders' => $hfolders + $folders, 'files' => $hfiles + $files);
}

/**
 * Recursively lists folders and files in this directory.
 * 
 * Similar to file_scan_directory(), except that we need the hierarchy.
 * Returns a sorted list which is compatible with theme('item_list') or
 * theme('media_cmdr'), folders first, then files.
 */
function _media_cmdr_list_files(&$params) {

  $url = _media_cmdr_parse_url($params['path'], $params['curr']);
  if ($url['proto'] == 'error')
    return array('error' => t('Cannot chdir to "!dir"<br/>URL parse error', array('!dir' => $url['all'])));

  if ($url['proto'] != 'file') return _media_cmdr_list_curl($url, $params);


  $list = array('dir' => preg_replace('!(.)/$!', '\1', $url['path']));
  $dir = 'public://' . $url['path'];

  if (!is_dir($dir) || !is_readable($dir) || !($handle = opendir($dir)))
    return array('error' => t('Cannot chdir to "!dir"<br/>No such directory', array('!dir' => $url['path'])));

//  $list = array();
  while (FALSE !== ($file = readdir($handle))) {
    $filename = preg_replace('|:///+|u', '://', "$dir/$file");
    if (!is_dir($filename) and !fnmatch($params['fltr'], $file)) continue;
    $list[] = array($file, filesize($filename), filemtime($filename),
       is_readable($filename), is_writable($filename), is_managed($filename),
       is_dir($filename));
  }
  closedir($handle);
  return _media_cmdr_build_list($list, $params);
}

function _media_cmdr_parse_url($path, $curr) {
  $x['rel'] = 0;
  if (!preg_match('|^([^:]+)://|', $path)) {  //relative path or file absolute path
    if ($path[0] != '/') {
      $path = $curr . '/' . $path;
      $x['rel'] = 1;  //relative path
    }
    if ($path[0] == '/') $path = 'file://' . $path;
  }
  if (preg_match('!^([^:]+)://(([^:@/]*)(:([^@/]*))?@)?([^/]+)?(/.*/(\.\.|\.)?)?(.*)?$!', $path, $m)) {
    $x['all'] = $m[0];
    $x['proto'] = $m[1];
    $x['user'] = $m[3];
    $x['pass'] = $m[5];
    $x['host'] = $m[6];
    $x['path'] = _media_cmdr_normalize_path($m[7]);
    $x['file'] = $m[9];
  }
  else {
    $x['all'] = $path;
    $x['proto'] = 'error';
    $x['user'] = $x['pass'] = $x['host'] = $x['path'] = $x['file'] = NULL;
  }
  return $x;
}

function _media_cmdr_normalize_path($path) {
  // bla//bloo ==> bla/bloo
  $path = preg_replace('|//+|', '/', $path);
  // bla/./bloo ==> bla/bloo
  $path = preg_replace('|/\.(/\.)*/|', '/', $path);
  // bla/.. ==> bla/../
  $path = preg_replace('|/\.\.$|', '/../', $path);
  // bla/. ==> bla
  $path = preg_replace('|/\.$|', '', $path);
  // resolve /../
  // loop through all the parts, popping whenever there's a .., pushing otherwise.
  $parts = array();
  foreach (explode('/', $path) as $part)
    if ($part === '..')
      array_pop($parts);
    else
      $parts[] = $part;
  $path = implode('/', $parts);
  if ($path == '') $path = '/';
  return $path;
}

function is_managed($uri) {
  $fid = db_query('SELECT fid FROM {file_managed} WHERE uri = :uri', array(':uri' => $uri))->fetchField();
  if (!empty($fid)) return TRUE;
  return FALSE;
}

function _media_cmdr_strcasecoll($string1, $string2) {
  return strcoll(mb_lowercase($string1), mb_lowercase($string2));
}

/**
 * Determines which icon should be displayed, based on file extension.
 */
function _media_cmdr_class($extension) {
  $extension = strtolower($extension);
  $icon = 'file';
  $map = array(
    'exe' => array('exe'),
//    'sym' => array('css'),
    'dba' => array('sql'),
    'doc' => array('doc', 'docx', 'xls', 'xlsx', 'odt', 'ods', 'ppt', 'pptx', 'odp', 'pdf', 'htm', 'html'),
    'vid' => array('avi', 'mov', 'flv', 'swf'),
    'aud' => array('mp3', 'aac', 'wav', 'au'),
    'src' => array('php', 'css', 'js', 'asp', 'pl'),
    'gra' => array('jpg', 'jpeg', 'gif', 'png', 'bmp'),
    'arc' => array('zip', 'gz', 'rar', 'arj', 'zoo', '7z'),
    'tmp' => array('~', 'tmp', 'temp'),
  );
  $class = 'unk';
  foreach ($map as $key => $values) {
    foreach ($values as $value) {
      if ($extension == $value) {
        $class = $key;
      }
    }
  }
  return $class;
}

/**
 *
 */
function _media_cmdr_list_curl($url, &$params) {
  if (!function_exists ('curl_init')) {
    return array('error' => t('Cannot chdir to "!dir"<br/>cURL service not installed', array('!dir' => $url['all'])));
  }
// create curl resource
  $ch = curl_init();
  foreach (preg_split('/\s*\n+\s*/u', $params['copt']) as $opt) {
    $opt = trim($opt);
    if ($opt == '') continue;
    list($k, $v) = explode('=', $opt);
    $k = 'CURLOPT_' . strtoupper($k);
    if (defined($k)) {
      if (curl_setopt($ch, constant($k), $v) === FALSE)
        return array('error' => t('Cannot chdir to "!dir"<br/>Could not set "!opt=!val"',
          array('!dir' => $url['all'],'!opt' => $k, '!val' => $v)));
    } else
      return array('error' => t('Cannot chdir to "!dir"<br/>cURL option "!opt" does not exists',
        array('!dir' => $url['all'], '!opt' => $k)));
  }
  if ($url['pass'] == '' && $params['pass'] != '') $url['pass'] = $params['pass'];
  if ($url['user'] != '') {  //set user/password
    curl_setopt($ch, CURLOPT_USERPWD, $url['user'] . ':' . $url['pass']);
  }
//return the transfer as a string
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
// set url
  curl_setopt($ch, CURLOPT_URL, $url['proto'] . '://' . $url['host'] . '/' . $url['path']);
// $output contains the output string
  $output = curl_exec($ch);
  $error = curl_error($ch);
// close curl resource to free up system resources
  curl_close($ch);

//  $list = array();
  $list = array('dir' => $url['proto'] . '://' . ($url['user'] ? $url['user'] . '@' : '') . $url['host'] . $url['path'], 'pass' => $url['pass']);
  if ($error != '')
    return array('error' => t('Cannot chdir to "!dir"<br/>cURL error: "!error"',
      array('!dir' => $url['proto'] . '://' . $url['host'] . $url['path'], '!error' => $error)));
  if ($url['proto'] == 'ftp' or $url['proto'] == 'sftp' or $url['proto'] == 'ftps' or $url['proto'] == 'scp') foreach (explode("\n", $output) as $file) {
  //-rw-r--r-- 1 www-data www-data  8877 Jun 30 13:54 media_cmdr.ajax.inc
    $data = array();
    if (preg_match('/^(.)(\S+)\s+\S+\s+\S+\s+\S+\s+(\S+)\s+(\S+\s+\S+\s+\S+)\s+(.+)$/u', $file, $m) and $m[5] != '') {
      $data[] = $m[5]; //filename
      $data[] = $m[3]; //filesize
      $data[] = _media_cmdr_parse_date($m[4]); //filetime
      $data[] = $m[2][6] == 'r' ? TRUE : FALSE; //readable
      $data[] = $m[2][7] == 'w' ? TRUE : FALSE; //writable
      $data[] = FALSE; //managed
      $data[] = $m[1] == 'd' ? TRUE : FALSE; //filename
      $list[] = $data;
    }
    elseif (preg_match('/^.([0-9-]+\s+\d\d:\d\d..)\s+(\S+)\s+(.+)$/u', $file, $m) and $m[3] != '') {
      $data[] = $m[3]; //filename
      $data[] = $m[2]; //filesize
      $data[] = _media_cmdr_parse_date($m[1]); //filetime
      $data[] = TRUE; //readable
      $data[] = TRUE; //writable
      $data[] = FALSE; //managed
      $data[] = $m[2] == '<DIR>' ? TRUE : FALSE; //filename
      $list[] = $data;
    }
  }
  elseif ($url['proto'] == 'http' or $url['proto'] == 'https') {
    if (preg_match('!<a href=".*">Parent Directory</a>.*-.*\n!i', $output)) {  //apache standard directory list
      foreach (explode("\n", preg_replace('!</?t[dr][^>]*>!', '', $output)) as $file) {
        $data = array();
        if (preg_match('!href\s*=\s*"[^"]+".+href\s*=\s*"[^"]+"!iu', $file) or
            preg_match('!<a href=".*">Parent Directory</a>!iu', $file)) continue;

        if (preg_match('!href\s*=\s*"([^"]*/)?([^/">]+)(/)?".*</a>\s*(\S+\s+\S+)\s+(\S+)!iu', $file, $m)) {
          $data[] = $m[2]; //filename
          $data[] = $m[5]; //filesize
          $data[] = _media_cmdr_parse_date($m[4]); //filetime
          $data[] = TRUE; //readable
          $data[] = FALSE; //writable
          $data[] = FALSE; //managed
          $data[] = $m[3] == '/' ? TRUE : FALSE; //filename
          $list[] = $data;
//          print "** " . $file . "<br/>\n";
        }
//        else
//          print "!! " . $file . "<br/>\n";
      }
    }
    else {
      preg_match_all('!href\s*=\s*"([^"]+)"!iu', preg_replace('/[\n\r\s]+/', ' ', $output), $match, PREG_SET_ORDER);
      foreach ($match as $m) {
        $data = array();
        $data[] = $m[1]; //filename
        $data[] = 0; //filesize
        $data[] = 0; //filetime
        $data[] = TRUE; //readable
        $data[] = FALSE; //writable
        $data[] = FALSE; //managed
        $data[] = preg_match('!(/|\.html?|\.php|\.asp)([\?#].*)?$!iu', $m[1]) ? TRUE : FALSE; //filename
//        print_r($data);
        $list[] = $data;
      }
    }
  }
  return _media_cmdr_build_list($list, $params);
}

function _media_cmdr_parse_date($str) {
  return strtotime($str);
}
