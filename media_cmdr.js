
/**
 * @file: Media Browser File Commander.
 *
 *
 */

/*
 * Queued Ajax requests.
 * A new Ajax request won't be started until the previous queued 
 * request has finished.
 */
jQuery.ajaxQueue = function(o){
  var _old = o.complete;
  o.complete = function(){
    if ( _old ) _old.apply( this, arguments );
    jQuery.dequeue( jQuery.ajaxQueue, "ajax" );
  };

  jQuery([ jQuery.ajaxQueue ]).queue("ajax", function() {
    jQuery.ajax( o );
  });
};


(function ($) {
namespace('Drupal.media.media_cmdr');

  Drupal.media.media_cmdr.dialog_active = null; //active dialog
  Drupal.media.media_cmdr.active = 'left'; //active panel
  Drupal.media.media_cmdr.curr = {left: 1, right:1}; //active row
  Drupal.media.media_cmdr.menu_pars = {
    left: {
      sort: 'n', 'case': true, rvrs: false, fltr: '*', hddn: true, rwm: '***', pass: ''
    },
    right: {
      sort: 'n', 'case': true, rvrs: false, fltr: '*', hddn: true, rwm: '***', pass: ''
    }
  }; //active row

  $(document).ready(function() {
//Disable ENTER submit form
//TODO: check other browser than FF5
    $('#media-tab-media_cmdr form input:text').attr('onkeypress', 'return event.keyCode!=13');
    $('#media-tab-media_cmdr form').keypress(function (event) {
        if ( event.keyCode == 9 || event.keyCode > 111 || event.keyCode < 128) {
             event.preventDefault();
        }
        var sel = Drupal.media.media_cmdr.active;
        var obj;
        switch (event.keyCode) {
          case 9:
            sel = sel == 'left' ? 'right' : 'left';
            Drupal.media.media_cmdr.set_panel(sel);
            break;
          case 38:  //Up arrow
            obj = $('#media-cmdr-' + sel + ' tr.active').prev('tr');
            obj.trigger('click');
            $('#media-cmdr-' + sel).scrollTop(
              obj.position().top -
                obj.parents('tbody').position().top -
                  obj.parents('div').outerHeight() / 2
            );
            break;
          case 40:  //Down arrow
            obj = $('#media-cmdr-' + sel + ' tr.active').next('tr');
            obj.trigger('click');
            $('#media-cmdr-' + sel).scrollTop(
              obj.position().top -
                obj.parents('tbody').position().top -
                  obj.parents('div').outerHeight() / 2
            );
            break;
          default:
            break;
        }
//        $('#devstatus').text('w: ' + event.which + ' c: ' + event.keyCode + ' shift: ' +
//           event.shiftKey + ' ctrl: ' + event.ctrlKey + ' meta: ' + event.metaKey + ' alt: ' + event.altKey);
//        var msg = "Handler for .keypress() called " + event.keyCode + " time(s).";
//        alert( msg );
    });
  //Set up Menu (F2)
    $('#media-cmdr-f2').click(Drupal.media.media_cmdr.fn_f2);
    $('.media-cmdr-menu-ok').click(Drupal.media.media_cmdr.fn_menu_ok);
    $('.media-cmdr-menu-cancel').click(Drupal.media.media_cmdr.fn_menu_cancel);
    $('#media-cmdr-menu-s').click(Drupal.media.media_cmdr.fn_menu_s);
    $('#media-cmdr-menu-f').click(Drupal.media.media_cmdr.fn_menu_f);
    $('.media-cmdr-rwm').click(Drupal.media.media_cmdr.fn_toggle_rwm);
    $('#media-cmdr-menu-c').click(Drupal.media.media_cmdr.fn_menu_c);
    $('#media-cmdr-menu-r').click(Drupal.media.media_cmdr.fn_menu_r);
  //Set up Copy (F5)
    $('#media-cmdr-f5').click(Drupal.media.media_cmdr.fn_f5);
    $('.media-cmdr-fileop-ok').click(Drupal.media.media_cmdr.fn_fileop_ok);
    $('.media-cmdr-fileop-cancel').click(Drupal.media.media_cmdr.fn_fileop_cancel);

  //Initialli load the two panel with files
    Drupal.media.media_cmdr.list('/', 'right');
    Drupal.media.media_cmdr.list('/', 'left');
  });

/*
  Drupal.media.media_cmdr.nosubmit = function (event) {
    if (event.keyCode == 13) {
      event.preventDefault();
      return false;
    }
  };
*/

// Menu (F2)

  Drupal.media.media_cmdr.fn_f2 = function() {
    if (!Drupal.media.media_cmdr.fn_tgldialog('menu', 'menu')) return;
    $('#media-cmdr-menu-menu caption').text('-- Menu ' + Drupal.media.media_cmdr.active + ' --');
    if (Drupal.media.media_cmdr.active == 'left') $('#media-cmdr-menu').css({'margin-left': '7%'});
    else $('#media-cmdr-menu').css({'margin-left': '62%'});
  };

  Drupal.media.media_cmdr.fn_menu_ok = function() {
    var d = $(this).parents('table.media-cmdr').attr('id');
    var refresh = true;
    switch (d) {
      case 'media-cmdr-menu-sort':
        Drupal.media.media_cmdr.fn_menu_s_save(); //save sort settings
        break;
      case 'media-cmdr-menu-fltr':
        Drupal.media.media_cmdr.fn_menu_f_save(); //save filter settings
        break;
      case 'media-cmdr-menu-cd':
        Drupal.media.media_cmdr.fn_menu_c_do(); //
        refresh = false;
        break;
    }
    Drupal.media.media_cmdr.fn_setdialog(null, null); //close dialog
    if (refresh) Drupal.media.media_cmdr.fn_menu_r();  //refresh and close menu
  }

  Drupal.media.media_cmdr.fn_menu_cancel = function() {
    Drupal.media.media_cmdr.fn_setdialog(null, null); //close dialog
  }

  Drupal.media.media_cmdr.fn_menu_s = function() {
    var sel = Drupal.media.media_cmdr.active;
    $('input[name="media-cmdr-menu-sort"][value=' + Drupal.media.media_cmdr.menu_pars[sel]['sort'] + ']').attr("checked", true);
    $('input[name="media-cmdr-menu-case"]').attr('checked', Drupal.media.media_cmdr.menu_pars[sel]['case']);
    $('input[name="media-cmdr-menu-rvrs"]').attr('checked', Drupal.media.media_cmdr.menu_pars[sel]['rvrs']);
    Drupal.media.media_cmdr.fn_setdialog('menu', 'sort');
  }
  Drupal.media.media_cmdr.fn_menu_s_save = function() {
    var sel = Drupal.media.media_cmdr.active;
    Drupal.media.media_cmdr.menu_pars[sel]['sort'] = $('input[name="media-cmdr-menu-sort"]:checked').val();
    Drupal.media.media_cmdr.menu_pars[sel]['case'] = $('input[name="media-cmdr-menu-case"]:checked').val() ? true : false;
    Drupal.media.media_cmdr.menu_pars[sel]['rvrs'] = $('input[name="media-cmdr-menu-rvrs"]:checked').val() ? true : false;
  }

  Drupal.media.media_cmdr.fn_menu_f = function() {
    var sel = Drupal.media.media_cmdr.active;
    $('input[name="media-cmdr-menu-fltr"]').val(Drupal.media.media_cmdr.menu_pars[sel]['fltr']);
    $('input[name="media-cmdr-menu-hddn"]').attr('checked', Drupal.media.media_cmdr.menu_pars[sel]['hddn']);
    $('#media-cmdr-menu-mode-r').text(Drupal.media.media_cmdr.menu_pars[sel]['rwm'][0]);
    $('#media-cmdr-menu-mode-w').text(Drupal.media.media_cmdr.menu_pars[sel]['rwm'][1]);
    $('#media-cmdr-menu-mode-m').text(Drupal.media.media_cmdr.menu_pars[sel]['rwm'][2]);
    Drupal.media.media_cmdr.fn_setdialog('menu', 'fltr');
  }
  Drupal.media.media_cmdr.fn_menu_f_save = function() {
    var sel = Drupal.media.media_cmdr.active;
    Drupal.media.media_cmdr.menu_pars[sel]['fltr'] = $('input[name="media-cmdr-menu-fltr"]').val();
    Drupal.media.media_cmdr.menu_pars[sel]['hddn'] = $('input[name="media-cmdr-menu-hddn"]:checked').val() ? true : false;
    Drupal.media.media_cmdr.menu_pars[sel]['rwm']  = $('#media-cmdr-menu-mode-r').text() + $('#media-cmdr-menu-mode-w').text() + $('#media-cmdr-menu-mode-m').text();
  }
  Drupal.media.media_cmdr.fn_toggle_rwm = function() {
    var d = $(this).text();
    switch (d) {
      case '*': d = '-'; break;
      case '-':
        if ($(this).hasClass('media-cmdr-mode-r')) d = 'r';
        else if ($(this).hasClass('media-cmdr-mode-w')) d = 'w';
        else d = 'm';
        break;
      default: d = '*';
    }
    $(this).text(d);
  }

  Drupal.media.media_cmdr.fn_menu_c = function() {
    var sel = Drupal.media.media_cmdr.active;
    Drupal.media.media_cmdr.fn_setdialog('menu', 'cd');
  }
  Drupal.media.media_cmdr.fn_menu_c_do = function() {
    var sel = Drupal.media.media_cmdr.active;
    var path = $('input[name="media-cmdr-menu-cd"]').val();
    $('input[name="media-cmdr-menu-cd"]').val('');
    Drupal.media.media_cmdr.list(path, sel);
    Drupal.media.media_cmdr.fn_f2(); //Close menu
  }

//Refresh
  Drupal.media.media_cmdr.fn_menu_r = function() {
    var sel = Drupal.media.media_cmdr.active;
    Drupal.media.media_cmdr.list('.', sel);
    Drupal.media.media_cmdr.fn_f2(); //Close menu
  }

// Copy (F5)
  Drupal.media.media_cmdr.fn_f5 = function() {
    if (!Drupal.media.media_cmdr.fn_tgldialog('fileop', 'fileop')) return;
    var sel = Drupal.media.media_cmdr.active;
    var oth = sel == 'left' ? 'right' : 'left';
    var topath = $('#media-cmdr-' + oth + '-path span').attr('title');

    $('#media-cmdr-fileop-fileop caption').text('--- Copy ---');
    $('#media-cmdr-fileop-op').text('Copy');
    $('input[name="media-cmdr-fileop-src"]').val('*');
    $('input[name="media-cmdr-fileop-dst"]').val( topath + (topath == '/' ? '' : '/'));
    if ($('#media-cmdr-' + sel + ' tr.selected').length > 0) {
      $('#media-cmdr-fileop-fn').text($('#media-cmdr-' + sel + ' tr.selected').length + ' files/directories');
    }
    else {
      $('#media-cmdr-fileop-fn').text('file "' + $('#media-cmdr-' + sel + ' tr.active td:first').text().substring(1) + '"');
    }
  };

  Drupal.media.media_cmdr.fn_fileop_ok = function() {
    Drupal.media.media_cmdr.fileop('copy');
    Drupal.media.media_cmdr.fn_fileop_cancel(); //TODO move to after complete fileops
  }

  Drupal.media.media_cmdr.fn_fileop_cancel = function() {
    Drupal.media.media_cmdr.fn_setdialog(null, null);
  }

  //Toggle the dialog
  Drupal.media.media_cmdr.fn_tgldialog = function(dlg, id) {
    if (Drupal.media.media_cmdr.fn_tgl_dlg(dlg, 2)) {
      Drupal.media.media_cmdr.fn_set_dlg(dlg, id);
      return true;
    }
    return false;
  }
  //Set up the dialog
  Drupal.media.media_cmdr.fn_setdialog = function(dlg, id) {
    if (dlg == null && id == null) {
      Drupal.media.media_cmdr.fn_tgl_dlg(dlg, 1);
      return;
    }
    if (!Drupal.media.media_cmdr.fn_tgl_dlg(dlg, 1)) return;
    Drupal.media.media_cmdr.fn_set_dlg(dlg, id);
  }

  Drupal.media.media_cmdr.fn_set_dlg = function(dlg, id) {
    var ids = ['menu', 'sort', 'fltr', 'cd', 'fileop', 'error'];
    for (i in ids) {
      d = 'media-cmdr-' + dlg + '-' + ids[i];
      if (ids[i] == id) {
        $('#media-cmdr-' + dlg).addClass(d);
        $('#' + d).show();
      }
      else {
        $('#' + d).hide();
        $('#media-cmdr-' + dlg).removeClass(d);
      }
    }
    if (id.substring(0,5) == 'error')
      $('#media-cmdr-' + dlg).addClass('media-cmdr-error');
    else
      $('#media-cmdr-' + dlg).removeClass('media-cmdr-error');
  }
  //Check there is a dialog open
  Drupal.media.media_cmdr.fn_tgl_dlg = function (dlg, set) {
    if (dlg === null) {
      if (set) {
        $('#media-cmdr-' + Drupal.media.media_cmdr.dialog_active).removeClass('active');
        Drupal.media.media_cmdr.dialog_active = null;
      }
      return 3;
    }
    else if (Drupal.media.media_cmdr.dialog_active === null) {
      if (set) {
        $('#media-cmdr-' + dlg).addClass('active');
        Drupal.media.media_cmdr.dialog_active = dlg;
      }
      return 1;
    }
    else if (Drupal.media.media_cmdr.dialog_active == dlg ) {
      if (set > 1) {
        $('#media-cmdr-' + dlg).removeClass('active');
        Drupal.media.media_cmdr.dialog_active = null;
      }
      return 2;
    }
    return 0;
  }

  //Set active panel and the cursor
  Drupal.media.media_cmdr.set_status = function (sel) {
    Drupal.media.media_cmdr.set_panel(sel);
    $('#media-cmdr-' + sel + ' tr.active').removeClass('active');
    $('#media-cmdr-' + sel + ' tr.selected').removeClass('selected');
    $('tr#' + sel + '-1').addClass('active');
    $('#media-cmdr-' + sel + '-status').text($('tr#' + sel + '-1').attr('title'));
  }

  //Add one filename
  Drupal.media.media_cmdr.list_fill_add = function (d, sel, pfx, n) {
    var cl = pfx == '/' ? 'd' : 'f';
    return '<tr id="' + sel + '-' + n + '" class="' + cl + ' ' + d[5] + '" title="' + d[0] + '">' +
      '<td class="n">' + pfx + d[1] +
      '</td><td class="s">' + d[2] + '</td><td class="d">' + d[3] +
      '</td><td class="m">' + d[4] + '</td></tr>';
  }

  //Add filenames to panel
  Drupal.media.media_cmdr.list_fill = function (data, sel) {
    if (data['error'] !== undefined) {
      $('#media-cmdr-menu-error tr:first td').html(data['error']);
      Drupal.media.media_cmdr.fn_setdialog('menu', 'error');
      return;
    }
    var n = 1;
    $('#media-cmdr-' + sel + '-path span').attr('title', data['dir'][0]);
    $('#media-cmdr-' + sel + '-path span').text(data['dir'][1]);
//    Drupal.media.media_cmdr.menu_pars[sel]['pass'] = data['pass'];
    $('input[name="media-cmdr-menu-pass"]').val(data['pass']);
    var tbody = Drupal.media.media_cmdr.list_fill_add(['..', '..', 'DIR--UP', '----------', '---', 'dir'], sel, '/', n++);
    for(var name in data['folders']) {
      d = data['folders'][name];
      tbody += Drupal.media.media_cmdr.list_fill_add(d, sel, '/', n++);
    };
    for(var name in data['files']) {
      d = data['files'][name];
      tbody += Drupal.media.media_cmdr.list_fill_add(d, sel, ' ', n++);
    };
//    $('#media-cmdr-' + sel + '-path span').text(data['dir']);
    $('#media-cmdr-' + sel + ' tbody').html(tbody);
    $('#media-cmdr-' + sel + ' tr').click( function() {
      if( Drupal.media.media_cmdr.dialog_active !== null) return;
      Drupal.media.media_cmdr.select(this);
    });
    $('#media-cmdr-' + sel + ' tr.dir').dblclick( function() {
      if( Drupal.media.media_cmdr.dialog_active !== null) return;
      Drupal.media.media_cmdr.select(this);
      var id = $(this).attr('id');
      var sel = id[0] == 'l' ? 'left' : 'right';
      var dir = $(this).attr('title');
      Drupal.media.media_cmdr.list(dir, sel);
    });
/* prevent selection */
    $('#media-cmdr-' + sel + ' tbody tr').mousedown( function() {
      return false;
    });
    Drupal.media.media_cmdr.set_status(sel);
  };

  Drupal.media.media_cmdr.set_panel = function (sel) {
    $('#media-tab-media_cmdr div.active').removeClass('active');
    $('#media-cmdr-' + sel + '-wrap').addClass('active');
    $('#media-cmdr-' + sel + '-wrap table:first th.n').attr('title', 'Filter: ' + Drupal.media.media_cmdr.menu_pars[sel]['fltr']);
    Drupal.media.media_cmdr.active = sel;
  };

  Drupal.media.media_cmdr.select = function (obj) {
    var id = $(obj).attr('id');
    var sel = id[0] == 'l' ? 'left' : 'right';
    if (sel != Drupal.media.media_cmdr.active) {
      Drupal.media.media_cmdr.set_panel(sel);
    }
    else {
      if ($(obj).hasClass('active')) $(obj).toggleClass('selected');
    }
    $('#media-cmdr-' + sel + '-status').text($(obj).attr('title'));
    $('.active tr.active').removeClass('active');
    $('tr#' + id ).addClass('active');
  };

  Drupal.media.media_cmdr.list = function (dir, sel) { // left / #right
    $('#media-cmdr-' + sel + '-status').text('Loading...');
    var params = Drupal.media.media_cmdr.menu_pars[sel];
    params['op'] = 'list';
    params['path'] = dir + '/';
    params['curr'] = $('#media-cmdr-' + sel + '-path span').attr('title');
    params['copt'] = $('textarea[name="media-cmdr-menu-copt"]').val();
    params['pass'] = $('input[name="media-cmdr-menu-pass"]').val();
    Drupal.media.media_cmdr.menu_pars[sel]['sort'];
    $.ajax({
      url: '?q=media/browser/cmdr',
      type: 'POST',
      dataType: 'json',
      data: params,
      error: function () {alert('Error getting data...') },
      success: function(data, status) {
        Drupal.media.media_cmdr.list_fill(data, sel);
      }
    });
  };


  Drupal.media.media_cmdr.fileop = function (op, params) { // left / right
    var sel = Drupal.media.media_cmdr.active;
    $('#media-cmdr-' + sel + '-status').text('Working...');
    var params = Drupal.media.media_cmdr.menu_pars[sel];
    params['op'] = op;
    params['curr'] = $('#media-cmdr-' + sel + '-path span').attr('title');
    params['src'] = $('input[name="media-cmdr-fileop-src"]').val();
    params['dst'] = $('input[name="media-cmdr-fileop-dst"]').val();
    params['msrc'] = $('input[name="media-cmdr-fileop-msrc"]').val() ? true : false;
    params['ptrn'] = $('input[name="media-cmdr-fileop-ptrn"]').val() ? true : false;
    params['dive'] = $('input[name="media-cmdr-fileop-dive"]').val() ? true : false;
    params['copt'] = $('textarea[name="media-cmdr-menu-copt"]').val();
    params['pass'] = $('input[name="media-cmdr-menu-pass"]').val();

    if ($('#media-cmdr-' + sel + ' tr.selected').length > 0) {
      $('#media-cmdr-' + sel + ' tr.selected').each(function () {
        params['file'] = $(this).attr('title');
        Drupal.media.media_cmdr.fileop_real(params);
      });
    }
    else {
      params['file'] = $('#media-cmdr-' + sel + ' tr.active').attr('title');
      Drupal.media.media_cmdr.fileop_real(params);
    }
  };

  Drupal.media.media_cmdr.fileop_real = function (params) { // left / right
    $.ajaxQueue($.ajax({
      url: '?q=media/browser/cmdr',
      type: 'POST',
      dataType: 'json',
      data: params,
      error: function () {alert('Error getting data...') },
      success: function(data, status) {
        Drupal.media.media_cmdr.fileop_next(data, sel);
      }
    }));
  };

})(jQuery);
