<?php

function _media_cmdr_parse_url($path, $curr) {
  $x['rel'] = 0;
  if (!preg_match('|^([^:]+)://|', $path)) {  //relative path or file absolute path
    if ($path[0] != '/') {
      $path = $curr . '/' . $path;
      $x['rel'] = 1;  //relative path
    }
    if ($path[0] == '/') $path = 'file://' . $path;
  }
  if (preg_match('!^([^:]+)://(([^:@/]*)(:([^@/]*))?@)?([^/]+)?(/.*/(\.\.|\.)?)?(.*)?$!', $path, $m)) {
    $x['all'] = $m[0];
    $x['proto'] = $m[1];
    $x['user'] = $m[3];
    $x['pass'] = $m[5];
    $x['host'] = $m[6];
    $x['path'] = _media_cmdr_normalize_path($m[7]);
    $x['file'] = $m[9];
  }
  else {
    $x['all'] = $path;
    $x['proto'] = 'error';
    $x['user'] = $x['pass'] = $x['host'] = $x['path'] = $x['file'] = NULL;
  }
  return $x;
}

  print_r(_media_cmdr_parse_url($argv[1], $argv[2]));
  return;
      print $argv[1] . "\n\n";
      preg_match_all('!href\s*=\s*"([^"]+)"!iu', preg_replace('/[\n\r\s]+/', ' ', $argv[1]), $match, PREG_SET_ORDER);
      foreach ($match as $m) {
        $data = array();
        $data[] = $m[1]; //filename
        $data[] = 0; //filesize
        $data[] = 0; //filetime
        $data[] = TRUE; //readable
        $data[] = FALSE; //writable
        $data[] = FALSE; //managed
        $data[] = preg_match('!(/|\.html?|\.php|\.asp)([\?#].*)?$!iu', $m[1]) ? TRUE : FALSE; //filename
        print_r($data);
      }

function _media_cmdr_normalize_path($path) {
  // bla//bloo ==> bla/bloo
  $path = preg_replace('|//+|', '/', $path);
  // bla/./bloo ==> bla/bloo
  $path = preg_replace('|/\.(/\.)*/|', '/', $path);
  // bla/.. ==> bla/../
  $path = preg_replace('|/\.\.$|', '/../', $path);
  // bla/. ==> bla
  $path = preg_replace('|/\.$|', '', $path);
  // resolve /../
  // loop through all the parts, popping whenever there's a .., pushing otherwise.
  $parts = array();
  foreach (explode('/', $path) as $part)
    if ($part === '..')
      array_pop($parts);
    else
      $parts[] = $part;
  $path = implode('/', $parts);
  if ($path == '') $path = '/';
  return $path;
}
?>